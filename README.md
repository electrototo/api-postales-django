# api-postales-django

Api de los códigos postales de México utilizando django, aws y zappa

## Arquitectura

La aplicación actual está desplegada en AWS utilizando zappa y un template de cloudformation. Fue necesario el template adicional de cloudformation para poder crear una base de datos en RDS y un Bucket de S3 para guardar los datos _sensibles_ del proyecto, como usuarios y contraseñas.

A continuación se puede ver la arquitectura de este proyecto:

![Arquitectura](https://gitlab.com/electrototo/api-postales-django/-/raw/2eea8bf9fcc8e74c93f3ce11c829cda9adb1a7ae/doc/images/architecture.png "Diagrama arquitectura")

## Descripción de la aplicación

En la aplicación están registrados dos endpoints principales: `/zip-codes/` y `/states/`. Se utilizó `django-rest-framework` para la creación del API, junto con `ViewSets` y `Routers`.

### states

Dentro de zip codes se pueden hacer todas las operaciones CRUD para obtener los estados almacenados en la base de datos.

### zip-codes

Dentro de zip codes se pueden hacer todas las operaciones CRUD para obtener los estados almacenados en la base de datos. La única diferencia es que cuando se quiere obtener la información de un codigo postal, el endpoint `/zip-codes/{codigo_postal}/` puede regresar más de un resultado, por lo que el parámetro `codigo_postal` no hace referencia a la llave primaria del registro de la base de datos. Esto es debido a que en la base de datos obtenida de [aquí](https://www.correosdemexico.gob.mx/SSLServicios/ConsultaCP/CodigoPostal_Exportar.aspx) hay lugares que cuentan con el mismo código postal, pero tienen un identificador único del asentamiento.

## Documentación de los endpoints

La documentanción de los endpoints fue autogenerada por `django-rest-framework` utilizando el formato `openapi` y se puede consultar accediendo al endpoint `/redoc`.

## Despliegue de la aplicación

Se tienen que seguir los siguientes pasos para desplegar de forma exitosa esta aplicación:
1. Se tiene que desplegar el stack de cloudformation definido en el archivo `stack.yml`.
2. Configurar zappa definiendo los parámetros de conexión a la base de datos creada en el paso 1, en el archivo `config_settings.json`.
3. Subir el archivo `config_settings.json` a la cubeta de S3 creada en el paso 1.
4. Desplegar la aplicación, `zappa deploy`.

### stack.yml

En este archivo de cloudformation se define tanto la base de datos en rds y la cubeta en la que va a estar almacenado el archivo de `config_settings.json`. Así mismo, se configura un security group rudimentario, el cual va a permitir todo el tráfico a todos los puertos de todas las IPs a la base de datos (no se recomiendo esto para un ambiente en producción).
Al desplegar el stack de cloudformation, el desarrollador tiene que ingresar el nombre de la base de datos a crear, el nombre del superusuario y la contraseña del mismo.
En este ambiente el superusuario de la base de datos y el usuario que usa Django para conectarse a la base de datos es el mismo, lo cual tampoco se recomienda en un ambiente en producción.

### config_settings.json

Este archivo JSON es el que va a ser utilizado por zappa para obtener los datos de conexión a la base de datos. En lo personal, no me gusta que se obtengan de esta manera los parámetros de conexión, ya que existen otros métodos para compartir secretos en AWS como: SSM Parameter Store, o AWS Secrets Manager.

El archivo debe estar estructurado de la siguiente manera:
```json
{
  "DB_HOST": "{URL de conexión a la base de datos}",
  "DB_USERNAME": "{Usuario de la base de datos}",
  "DB_PASSWORD": "{Password del usuario}",
  "DB_PORT": "{Puerto de la base de datos}",
  "DB_NAME": "{Nombre de la base de datos}"
}
```
