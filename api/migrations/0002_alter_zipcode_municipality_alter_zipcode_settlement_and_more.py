# Generated by Django 4.0.4 on 2022-04-27 12:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='zipcode',
            name='municipality',
            field=models.CharField(max_length=128),
        ),
        migrations.AlterField(
            model_name='zipcode',
            name='settlement',
            field=models.CharField(max_length=128),
        ),
        migrations.AlterField(
            model_name='zipcode',
            name='settlement_type',
            field=models.CharField(max_length=128),
        ),
    ]
