from django.db import models


class State(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class ZipCode(models.Model):
    code = models.CharField(max_length=10)

    settlement = models.CharField(max_length=128)

    settlement_type = models.CharField(max_length=128)

    municipality = models.CharField(max_length=128)

    zone = models.CharField(max_length=32)

    state = models.ForeignKey(
        State,
        on_delete=models.PROTECT
    )

    asenta_id = models.IntegerField()

    def __str__(self):
        return self.code
