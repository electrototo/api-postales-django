from rest_framework import serializers

from api.models import State, ZipCode


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = ['id', 'name']


class ZipCodeSerializer(serializers.ModelSerializer):
    state = StateSerializer(read_only=True)

    class Meta:
        model = ZipCode
        fields = [
            'id', 'code', 'settlement', 'settlement_type',
            'municipality', 'zone', 'state'
        ]


class CreateZipCSerializer(serializers.ModelSerializer):
    class Meta:
        model = ZipCode
        fields = '__all__'
