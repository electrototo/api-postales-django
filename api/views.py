from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api import serializers
from api.models import State, ZipCode


class StateViewSet(viewsets.ModelViewSet):
    queryset = State.objects.all()
    serializer_class = serializers.StateSerializer


class ZipCodesViewSet(viewsets.ModelViewSet):
    queryset = ZipCode.objects.all()
    serializer_class = serializers.ZipCodeSerializer

    @action(detail=False, methods=['get'])
    def get_zipcode(self, request, zip_code=None):
        qs = ZipCode.objects.filter(code=zip_code)

        page = self.paginate_queryset(qs)
        if page is not None:
            serializer = self.get_serializer(page, many=True)

            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(qs, many=True)

        return Response(serializer.data)

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return serializers.CreateZipCSerializer

        return super().get_serializer_class()
