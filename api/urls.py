from django.urls import path
from django.views.generic import TemplateView

from rest_framework import routers
from rest_framework.schemas import get_schema_view

from api import views


urlpatterns = [
    path('zip-codes/<str:zip_code>', views.ZipCodesViewSet.as_view({'get': 'get_zipcode'})),
    path('openapi', get_schema_view(), name='openapi-schema'),
    path('redoc/', TemplateView.as_view(
        template_name='redoc.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='redoc')
]


router = routers.SimpleRouter()
router.register('states', views.StateViewSet)
router.register('zip-codes', views.ZipCodesViewSet)

urlpatterns += router.urls
